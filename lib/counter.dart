import 'package:flutter/material.dart';
import 'dart:math';
import 'button_change.dart';
import 'counter_display.dart';


class Counter extends StatefulWidget {
  @override
  _CounterState createState() => _CounterState();
}

class _CounterState extends State<Counter> {
  int _counter = 0;

  void _changeCounter(int val){
    print('_CounterState: Start SetState with from val = ' + val.toString());
    setState(()=> _counter += val);
    print('_CounterState: End SetState with to val = ' + val.toString());
  }

  @override
  Widget build(BuildContext context) {
    print('_CounterState: Re-build context with current counter = ' + _counter.toString());

    Color _randomColor = Colors.primaries[Random().nextInt(Colors.primaries.length)];

    return
      Scaffold(
        appBar: AppBar(
          title: Text('Demo State Management'),
          backgroundColor: Colors.green,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  ButtonChangeCounter(
                    text: '-',
                    buttonChangeCounterPressed: () {
                      print('_CounterState: Start Pressed button with Changed -');
                      _changeCounter(-1);
                      print('_CounterState: End Pressed button with Changed -');
                    },
                    backgroundColor: Colors.red,
                    textColor: _randomColor,
                  ),
                  SizedBox(width: 16),
                  CounterDisplay(val: _counter),
                  SizedBox(width: 16),
                  ButtonChangeCounter(
                    text: '+',
                    buttonChangeCounterPressed: () {
                        print('_CounterState: Start Pressed button with Changed +');
                        _changeCounter(1);
                        print('_CounterState: End Pressed button with Changed +');
                    },
                    backgroundColor: Colors.green,
                    textColor: _randomColor,
                  )
                ],
              )
            ],
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.miniCenterFloat,
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.switch_left),
          backgroundColor: Colors.green,
          onPressed: (){_changeCounter(1);},
        ),
      );
  }
}