import 'package:flutter/material.dart';

class CounterDisplay extends StatelessWidget {
  CounterDisplay({required this.val});

  final int val;

  @override
  Widget build(BuildContext context) {
    return Text('Counter = $val');
  }
}