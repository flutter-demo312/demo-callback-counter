import 'package:flutter/material.dart';
  typedef VoidCallBackText = void Function();
  typedef FunctionCallBackText = String Function(int);


class ButtonChangeCounter extends StatelessWidget {
  ButtonChangeCounter({required this.text, required this.buttonChangeCounterPressed, required this.backgroundColor, required this.textColor});

  //Su dung Typedef VoidCallBack Standard
  //final VoidCallback onPressedButton;
  final VoidCallBackText buttonChangeCounterPressed;
  final String text;
  final Color backgroundColor;
  final Color textColor;

  @override
  Widget build(BuildContext context) {
    print('ButtonChangeCounter: Re-build context ButtonChangeCounter ' + this.text.toString()+' with color='+textColor.toString());
    return ElevatedButton(
      onPressed: () {
        print('ButtonChangeCounter: Start - Pressed button in ButtonChangeCounter with changed '+ this.text);
        buttonChangeCounterPressed();
        print('ButtonChangeCounter: End - Pressed button in ButtonChangeCounter with changed '+ this.text);
      },
      child: Text(
          this.text,
          style: TextStyle(
            color: this.textColor,
            fontWeight: FontWeight.bold,
            fontSize:  50.0
          ),
      ),
      style: ElevatedButton.styleFrom(
        primary: this.backgroundColor
      ),
    );
  }
}